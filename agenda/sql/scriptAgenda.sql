DROP DATABASE `agenda`;

CREATE DATABASE `agenda`;
USE agenda;

CREATE TABLE `localidades`
(
  `idLocalidad` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
   PRIMARY KEY (`idLocalidad`)
);

CREATE TABLE `tiposContactos`
(
  `idTipoContacto` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipoContacto`)
);
  
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `idLocalidad` int(11),
  `idTipoContacto` int (11),
  `Nombre` varchar(50) NOT NULL,
  `Telefono` varchar(50) NOT NULL,
  `Calle` varchar(50),
  `Altura` varchar(50),
  `Piso` varchar(50),
  `Dpto` varchar(50),
  `Mail` varchar(50),
  `FechaCumpleanios` varchar(50),
  PRIMARY KEY (`idPersona`),
  INDEX (`idLocalidad`),
  FOREIGN KEY (`idLocalidad`) REFERENCES `localidades`(`idLocalidad`),
  FOREIGN KEY (`idTipoContacto`) REFERENCES `tiposContactos`(`idTipoContacto`)
);

insert into	`localidades` (`idLocalidad`,`nombre`) values(0,'default');
insert into	`localidades` (`idLocalidad`,`nombre`) values(0,'Polvorines');
insert into	`localidades` (`idLocalidad`,`nombre`) values(0,'Torcuato');
insert into	`localidades` (`idLocalidad`,`nombre`) values(0,'Lemos');
insert into	`localidades` (`idLocalidad`,`nombre`) values(0,'San Miguel');
insert into	`localidades` (`idLocalidad`,`nombre`) values(0,'San Fernando');

insert into `tiposContactos` (`idTipoContacto`,`Nombre`) values(0,'default');
insert into `tiposContactos` (`idTipoContacto`,`Nombre`) values(0,'Familia');
insert into `tiposContactos` (`idTipoContacto`,`Nombre`) values(0,'Trabajo');