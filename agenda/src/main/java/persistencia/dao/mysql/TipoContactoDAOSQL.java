package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;
import dto.TipoContactoDTO;

public class TipoContactoDAOSQL implements TipoContactoDAO
{
	private static final String insert = "INSERT INTO tiposContactos(idTipoContacto, Nombre) VALUES(?, ?)";
	private static final String delete = "DELETE FROM tiposContactos WHERE idTipoContacto = ?";
	private static final String readall = "SELECT * FROM tiposContactos";
	private static final String readone = "SELECT * FROM tiposContactos t WHERE t.idTipoContacto = ?";
	private static final String edit = "UPDATE tiposContactos tc SET tc.nombre = ? WHERE tc.idTipoContacto = ?";

	private static DAOAbstractFactory DAOfactory = new DAOSQLFactory();
	private static PersonaDAO personas = DAOfactory.createPersonaDAO();
		
	public boolean insert(TipoContactoDTO tipoContacto)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, tipoContacto.getIdTipoContacto());
			statement.setString(2, tipoContacto.getNombre());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(TipoContactoDTO tipoDeContacto_a_eliminar)
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		
		if(tipoDeContacto_a_eliminar.getIdTipoContacto()!=1)
		{
			try 
			{
				personas.deleteTC(tipoDeContacto_a_eliminar);
				
				statement = conexion.getSQLConexion().prepareStatement(delete);
				statement.setString(1, Integer.toString(tipoDeContacto_a_eliminar.getIdTipoContacto()));
				chequeoUpdate = statement.executeUpdate();
				if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
					return true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public List<TipoContactoDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<TipoContactoDTO> tiposContactos = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				tiposContactos.add(new TipoContactoDTO(resultSet.getInt("idTipoContacto"), resultSet.getString("Nombre")));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tiposContactos;
	}

	public TipoContactoDTO readOne(int id) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		TipoContactoDTO tipoContacto = null;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readone);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				tipoContacto = new TipoContactoDTO(resultSet.getInt("idTipoContacto"), resultSet.getString("Nombre"));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return tipoContacto;
	}

	@Override
	public boolean edit(TipoContactoDTO tipoContacto_a_editar, TipoContactoDTO tipoContacto_actualizado) {
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(edit);
			statement.setString(1, tipoContacto_actualizado.getNombre());
			statement.setInt(2, tipoContacto_a_editar.getIdTipoContacto());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
		
	}

}
