package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, idLocalidad, idTipoContacto, nombre, telefono, calle, altura, piso, dpto, mail, fechaCumpleanios) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas";
	private static final String edit = "UPDATE personas p SET p.idLocalidad = ?, p.idTipoContacto = ?, p.Nombre = ?, p.Telefono = ?, p.Calle = ?, p.Altura = ?, p.Piso = ?, p.Dpto = ?, p.Mail = ?, p.fechaCumpleanios = ? WHERE p.idPersona = ?";
	private static final String deleteLoc = "UPDATE personas p SET p.idLocalidad = 1 WHERE p.idLocalidad = ?";
	private static final String deleteTC = "UPDATE personas p SET p.idTipoContacto = 1 WHERE p.idTipoContacto  = ?";
	
	private static DAOAbstractFactory DAOfactory = new DAOSQLFactory();
	private static LocalidadDAO localidades = DAOfactory.createLocalidadDAO();
	private static TipoContactoDAO tiposContactos = DAOfactory.createTipoContactoDAO();
	
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setInt(2, persona.getLocalidad().getIdLocalidad());
			statement.setInt(3,persona.getTipoContacto().getIdTipoContacto());
			statement.setString(4, persona.getNombre());
			statement.setString(5, persona.getTelefono());
			statement.setString(6, persona.getCalle());
			statement.setString(7, persona.getAltura());
			statement.setString(8, persona.getPiso());
			statement.setString(9, persona.getDpto());
			statement.setString(10, persona.getMail());
			statement.setString(11, persona.getFechaCumpleaños());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				personas.add(new PersonaDTO(
						resultSet.getInt("idPersona"),
						localidades.readOne(resultSet.getInt("idLocalidad")),
						tiposContactos.readOne(resultSet.getInt("idTipoContacto")),
						resultSet.getString("Nombre"),
						resultSet.getString("Telefono"),
						resultSet.getString("Calle"),
						resultSet.getString("Altura"),
						resultSet.getString("Piso"),
						resultSet.getString("Dpto"),
						resultSet.getString("Mail"),
						resultSet.getString("FechaCumpleanios")
						));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}

	@Override
	public boolean edit(PersonaDTO persona_a_editar, PersonaDTO persona_actualizada) {
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(edit);
			statement.setInt(1, persona_actualizada.getLocalidad().getIdLocalidad());
			statement.setInt(2,persona_actualizada.getTipoContacto().getIdTipoContacto());
			statement.setString(3, persona_actualizada.getNombre());
			statement.setString(4, persona_actualizada.getTelefono());
			statement.setString(5, persona_actualizada.getCalle());
			statement.setString(6, persona_actualizada.getAltura());
			statement.setString(7, persona_actualizada.getPiso());
			statement.setString(8, persona_actualizada.getDpto());
			statement.setString(9, persona_actualizada.getMail());
			statement.setString(10, persona_actualizada.getFechaCumpleaños());
			statement.setInt(11, persona_a_editar.getIdPersona());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
		
	}

	@Override
	public boolean deleteLoc(LocalidadDTO l) {

		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(deleteLoc);
			statement.setInt(1,l.getIdLocalidad());
			
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean deleteTC(TipoContactoDTO t) {
		
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(deleteTC);
			statement.setInt(1,t.getIdTipoContacto());
			
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

}
