package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public interface PersonaDAO 
{
	
	public boolean insert(PersonaDTO persona);

	public boolean delete(PersonaDTO persona_a_eliminar);
	
	public List<PersonaDTO> readAll();

	public boolean edit(PersonaDTO p, PersonaDTO nuevaPersona);
	
	public boolean deleteLoc(LocalidadDTO l);
	
	public boolean deleteTC(TipoContactoDTO t);
}
