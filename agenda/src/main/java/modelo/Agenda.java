package modelo;

import java.util.List;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;



public class Agenda 
{
	private PersonaDAO persona;	
	private LocalidadDAO localidad;
	private TipoContactoDAO tipos;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.tipos = metodo_persistencia.createTipoContactoDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}
	
	public void editarPersona(PersonaDTO personaAnterior, PersonaDTO personaActualizada){
		
		this.persona.edit(personaAnterior, personaActualizada);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	public void agregarLocalidad(LocalidadDTO nuevaLocalidad)
	{
		this.localidad.insert(nuevaLocalidad);
	}

	public void borrarLocalidad(LocalidadDTO localidad_a_eliminar) 
	{
		this.localidad.delete(localidad_a_eliminar);
	}
	
	public List<LocalidadDTO> obtenerLocalidades()
	{
		return this.localidad.readAll();		
	}
	
	public LocalidadDTO buscarLocalidadxId(int id){
		
		return this.localidad.readOne(id);
	}

	public void agregarTipoContacto(TipoContactoDTO nuevoTipo){
		
		this.tipos.insert(nuevoTipo);
	}
	
	public void borrarTipoContacto(TipoContactoDTO tipo_a_eliminar){
		
		this.tipos.delete(tipo_a_eliminar);
	}
	
	public List<TipoContactoDTO> obtenerTiposContactos(){
		
		return this.tipos.readAll();
	}
	
	public TipoContactoDTO buscarTipoxId(int id){
		
		return this.tipos.readOne(id);
	}

	public void editarLocalidad(LocalidadDTO localidadAnterior,LocalidadDTO localidadActualizada) {
		this.localidad.edit(localidadAnterior, localidadActualizada);
		
	}

	public void editarTipoContacto(TipoContactoDTO tipoDeContactoAnterior,TipoContactoDTO TipoContactoActualizado) {
		this.tipos.edit(tipoDeContactoAnterior, TipoContactoActualizado);
		
	}
}
