package dto;

public class TipoContactoDTO 
{
	private int idTipoDeContacto;
	private String nombre;


	public TipoContactoDTO(int id, String tipo)
	{
		this.idTipoDeContacto = id;
		this.nombre = tipo;
	}


	public void setIdTipoDeContacto(int idTipoDeContacto) {
		this.idTipoDeContacto = idTipoDeContacto;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String tipo) {
		this.nombre = tipo;
	}


	public int getIdTipoContacto() {
		return idTipoDeContacto;
	}
}