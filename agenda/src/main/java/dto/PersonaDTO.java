package dto;

public class PersonaDTO 
{
	private int idPersona;
	private LocalidadDTO localidad;
	private TipoContactoDTO tipoContacto;
	private String nombre;
	private String telefono;
	private String calle;
	private String altura;
	private String piso;
	private String dpto;
	private String mail;
	private String fechaCumpleaños;

	public PersonaDTO(int idPersona, LocalidadDTO localidad, TipoContactoDTO tipoContacto, String nombre, String telefono, String calle, String altura, String piso, String dpto, String mail, String fechaCumlpeaños)
	{
		this.idPersona = idPersona;
		this.localidad = localidad;
		this.tipoContacto = tipoContacto;
		this.nombre = nombre;
		this.telefono = telefono;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.dpto = dpto;
		this.mail = mail;
		this.fechaCumpleaños = fechaCumlpeaños;
	}
	
	public String getFechaCumpleaños() {
		return fechaCumpleaños;
	}

	public void setFechaCumpleaños(String fechaCumpleaños) {
		this.fechaCumpleaños = fechaCumpleaños;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public LocalidadDTO getLocalidad() {
		return localidad;
	}

	public void setLocalidad(LocalidadDTO localidad) {
		this.localidad = localidad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}


	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}

	public TipoContactoDTO getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(TipoContactoDTO tipoContacto) {
		this.tipoContacto = tipoContacto;
	}
}
