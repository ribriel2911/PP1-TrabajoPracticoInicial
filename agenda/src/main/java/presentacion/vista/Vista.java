package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import persistencia.conexion.Conexion;

public class Vista
{
	private JFrame frame;
	private JPanel panel;
	JScrollPane spPersonas;
	private JTable tablaPersonas;
	private JTable tablaTiposDeContactos;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnAgregarLocalidad;	
	private JButton btnAgregarTipoContacto;
	private DefaultTableModel modelPersonas;
	private DefaultTableModel modelLocalidades;
	private DefaultTableModel modelTiposDeContactos;
	private  String[] nombreColumnas = {"Nombre y apellido","Tipo de Contacto","Telefono","Direccion","Piso","Dpto","Localidad","Mail", "Fecha Cumpleaños"};
	private  String[] nombreColumnasL = {"Id","Nombre"};
	private  String[] nombreColumnasTC = {"Id","Tipo"};

	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frame = new JFrame();
		frame.setTitle("Agenda");
		frame.setBounds(0, 0, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		spPersonas = new JScrollPane();
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);

		modelLocalidades = new DefaultTableModel(null,nombreColumnasL);
		
		modelTiposDeContactos = new DefaultTableModel(null,nombreColumnasTC);
		tablaTiposDeContactos = new JTable(modelTiposDeContactos);
		
		tablaPersonas.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tablaPersonas.setModel(new ModeloTabla(tablaPersonas.getModel()));
		
		spPersonas.setViewportView(tablaPersonas);
		//spLocalidades.setViewportView(tablaLocalidades);
		//spLocalidades.setViewportView(tablaTiposDeContactos);
		
		btnAgregar = new JButton("Agregar");
		panel.add(btnAgregar);
		
		btnAgregarLocalidad = new JButton("Localidad");
		panel.add(btnAgregarLocalidad);
		
		btnAgregarTipoContacto = new JButton("Tipo De Contacto");
		panel.add(btnAgregarTipoContacto);

		
		btnEditar = new JButton("Editar");
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		panel.add(btnReporte);
		
		resize();
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Estas seguro que quieres salir de la Agenda!?", 
		             "Confirmacion", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public JButton getBtnAgregarLocaliadad() 
	{
		return btnAgregarLocalidad;
	}
	
	public JButton getBtnAgregarTipoDeContacto() 
	{
		return btnAgregarTipoContacto;
	}
	
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public DefaultTableModel getModelLocalidades() 
	{
		return modelLocalidades;
	}
	
	public DefaultTableModel getModelTiposDeContactos() 
	{
		return modelTiposDeContactos;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}
	
	public JTable getTablaTiposDeContactos()
	{
		return tablaTiposDeContactos;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public String[] getNombreColumnasL() 
	{
		return nombreColumnasL;
	}
	
	public String[] getNombreColumnasTC() 
	{
		return nombreColumnasTC;
	}
	
	public JFrame getFrame(){
		
		return this.frame;
 	}
	
	public void resize(){
		
		panel.setBounds(0, 0, frame.getWidth()-16, frame.getHeight()-38);
		spPersonas.setBounds(10, 11, panel.getWidth()-20, panel.getHeight()-80);
		tablaPersonas.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		btnAgregar.setBounds(10, panel.getHeight()-62, 89, 23);
		btnEditar.setBounds(109, panel.getHeight()-62, 89, 23);
		btnBorrar.setBounds(10, panel.getHeight()-34, 89, 23);
		btnReporte.setBounds(109, panel.getHeight()-34, 89, 23);
		btnAgregarLocalidad.setBounds(panel.getWidth()- 198, panel.getHeight()-62 , 188, 23);
		btnAgregarTipoContacto.setBounds(panel.getWidth()- 198, panel.getHeight()-34, 188, 23);
	}
}
