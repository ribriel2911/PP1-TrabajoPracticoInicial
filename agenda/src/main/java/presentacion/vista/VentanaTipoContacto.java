package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import presentacion.controlador.ControladorBotones;

public class VentanaTipoContacto extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtTipo;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	JScrollPane spTipoContacto;
	private JTable tablaTipoContacto;
	private DefaultTableModel modelTipoContacto;
	private ControladorBotones controlador;
	public Object getTxtId;
	private  String[] nombreColumnasTC = {"Nombre"};


	public VentanaTipoContacto(Vista v,ControladorBotones controlador) 
	{
		super(v.getFrame());
		this.controlador = controlador;
		this.setTitle("Tipo de contacto");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 300);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 500, 300);
		contentPane.add(panel);
		panel.setLayout(null);
		
		spTipoContacto = new JScrollPane();
		panel.add(spTipoContacto);
		
		modelTipoContacto = new DefaultTableModel(null,nombreColumnasTC);
		tablaTipoContacto = new JTable(modelTipoContacto);
		
		tablaTipoContacto.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tablaTipoContacto.setModel(new ModeloTabla(tablaTipoContacto.getModel()));
		
		
		spTipoContacto.setViewportView(tablaTipoContacto);
		
		JLabel lblCalle = new JLabel("Nombre");
		lblCalle.setBounds(10, 11, 113, 14);
		panel.add(lblCalle);
		
		txtTipo = new JTextField();
		txtTipo.setBounds(150, 160, 164, 20);
		panel.add(txtTipo);
		txtTipo.setColumns(10);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(this.controlador);
		btnAgregar.setBounds(100, 200, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(this.controlador);
		btnEditar.setBounds(200, 200, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(this.controlador);
		btnBorrar.setBounds(300, 200, 89, 23);
		panel.add(btnBorrar);
		
//		panel.setBounds(0, 0, frame.getWidth()-16, frame.getHeight()-38);
		spTipoContacto.setBounds(10, 11, panel.getWidth()-50, panel.getHeight()-160);
		tablaTipoContacto.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		this.setVisible(true);
	}
	
	public JTextField getTxtTipo() 
	{
		return txtTipo;
	}

	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public DefaultTableModel getModelTiposDeContactos() 
	{
		return modelTipoContacto;
	}
	public String[] getNombreColumnasTC() 
	{
		return nombreColumnasTC;
	}
	
	public JTable getTablaTiposDeContactos()
	{
		return tablaTipoContacto;
	}
}