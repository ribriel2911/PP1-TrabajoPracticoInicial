package presentacion.vista;

import java.awt.Color;
import java.time.LocalDate;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import com.github.lgooddatepicker.components.DatePicker;

import dto.PersonaDTO;
import presentacion.controlador.ControladorBotones;

public class VentanaPersona extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDpto;
	private JTextField txtEmail;
	private DatePicker dateCumpleaños;
	private JComboBox<String> combo;
	private JComboBox<String> combo2;
	private JButton boton;
	private ControladorBotones controlador;

	public VentanaPersona(Vista v,ControladorBotones controlador) 
	{
		super(v.getFrame());
		this.controlador = controlador;
		this.setTitle("Agregar Contacto");
		init();
	}
	
	public void CargarPersona(PersonaDTO p){
		
		this.setTitle("Editar Contacto");
		
		boton.setText("Editar");
		
		setTxtNombre(p.getNombre());
		setTxtTelefono(p.getTelefono());
		setTxtCalle(p.getCalle());
		setTxtAltura(p.getAltura());
		setTxtPiso(p.getPiso());
		setTxtDpto(p.getDpto());
		setTxtEmail(p.getMail());
		setCombo(p.getLocalidad().getIdLocalidad()-1);
		setCombo2(p.getTipoContacto().getIdTipoContacto()-1);
		this.getDateCumpleaños().setDate(LocalDate.of(	Integer.parseInt(p.getFechaCumpleaños().substring(0, 4)),
														Integer.parseInt(p.getFechaCumpleaños().substring(5, 7)),
														Integer.parseInt(p.getFechaCumpleaños().substring(8, 10))));
	}
	
	public void init(){
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 360, 380);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 360, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		
		//LABELS
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 113, 19);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 41, 113, 19);
		panel.add(lblTelfono);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 71, 113, 19);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 101, 113, 19);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 131, 113, 19);
		panel.add(lblPiso);
		
		JLabel lblDpto = new JLabel("Dpto");
		lblDpto.setBounds(10, 161, 113, 19);
		panel.add(lblDpto);
		
		JLabel lblEmail = new JLabel("e-mail");
		lblEmail.setBounds(10, 191, 113, 19);
		panel.add(lblEmail);
		
		JLabel lblCumpleanios = new JLabel("Cumpleaños");
		lblCumpleanios.setBounds(10, 221, 113, 19);
		panel.add(lblCumpleanios);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 251, 113, 19);
		panel.add(lblLocalidad);
		
		JLabel lblTipoContacto = new JLabel("Tipo De Contacto");
		lblTipoContacto.setBounds(10, 281, 113, 19);
		panel.add(lblTipoContacto);
				
		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 10, 172, 25);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 40, 172, 25);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(133, 70, 172, 25);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField();
		txtAltura.setBounds(133, 100, 172, 25);
		panel.add(txtAltura);
		txtAltura.setColumns(10);
		
		txtPiso = new JTextField();
		txtPiso.setBounds(133, 130, 172, 25);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		txtDpto = new JTextField();
		txtDpto.setBounds(133, 160, 172, 25);
		panel.add(txtDpto);
		txtDpto.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(133, 190, 172, 25);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		dateCumpleaños = new DatePicker();
		dateCumpleaños.setBounds(133, 220, 200, 25);
		
		JTextField txtCumpleaños = dateCumpleaños.getComponentDateTextField();
		txtCumpleaños.setText("Seleccione Fecha");
		txtCumpleaños.setDisabledTextColor(Color.RED);
		txtCumpleaños.setEnabled(false);
		dateCumpleaños.getComponents()[0] = txtCumpleaños;
		panel.add(dateCumpleaños);
		
		combo = new JComboBox<String>();
		
		combo.setBounds(133, 250, 200, 25);
		panel.add(combo);
		
		combo2 = new JComboBox<String>();
		
		combo2.setBounds(133, 280, 200, 25);
		panel.add(combo2);
		
		//BOTON
		boton = new JButton("Agregar");
		boton.addActionListener(this.controlador);
		boton.setBounds(208, 310, 89, 25);
		panel.add(boton);
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public void setTxtNombre(String nombre){
		
		txtNombre.setText(nombre);
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	
	public void setTxtTelefono(String telefono){
		
		txtTelefono.setText(telefono);
	}
	
	public JTextField getTxtCalle() 
	{
		return txtCalle;
	}
	
	public void setTxtCalle(String calle){
		
		txtCalle.setText(calle);
	}
	
	public JTextField getTxtAltura() 
	{
		return txtAltura;
	}
	
	public void setTxtAltura(String altura){
		
		txtAltura.setText(altura);
	}
	
	
	public JTextField getTxtPiso() 
	{
		return txtPiso;
	}
	
	public void setTxtPiso(String piso){
		
		txtPiso.setText(piso);
	}
	
	public JTextField getTxtDpto() 
	{
		return txtDpto;
	}
	
	public void setTxtDpto(String dpto){
		
		txtDpto.setText(dpto);
	}
	
	public DatePicker getDateCumpleaños() {
		return dateCumpleaños;
	}

	public void setDateCumpleaños(DatePicker dateCumpleaños) {
		this.dateCumpleaños = dateCumpleaños;
	}

	public JTextField getTxtEmail() 
	{
		return txtEmail;
	}
	
	public void setTxtEmail(String email){
		
		txtEmail.setText(email);
	}

	public JComboBox<String> getCombo() {
		return combo;
	}
	
	public void setCombo(int id){
		this.combo.setSelectedIndex(id);
	}

	public void addToCombo(String string) {
		this.combo.addItem(string);
	}
	
	public JComboBox<String> getCombo2() {
		return combo2;
	}
	
	public void setCombo2(int id){
		this.combo2.setSelectedIndex(id);
	}

	public void addToCombo2(String string) {
		this.combo2.addItem(string);
	}

	public JButton getBoton() 
	{
		return boton;
	}
}

