package presentacion.vista;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class ModeloTabla implements TableModel{
	
	private TableModel base;
	
	public ModeloTabla(TableModel base){
		
		this.base = base;
	}

	@Override
	public void addTableModelListener(TableModelListener l) {

		base.addTableModelListener(l);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {

		return base.getColumnClass(columnIndex);
	}

	@Override
	public int getColumnCount() {

		return base.getColumnCount();
	}

	@Override
	public String getColumnName(int columnIndex) {

		return base.getColumnName(columnIndex);
	}

	@Override
	public int getRowCount() {

		return base.getRowCount();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		return base.getValueAt(rowIndex, columnIndex);
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		
		base.removeTableModelListener(l);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		
		base.setValueAt(aValue, rowIndex, columnIndex);
	}
	
	@Override
	public boolean isCellEditable (int row, int column)
	   {
	       return false;
	   }
}
