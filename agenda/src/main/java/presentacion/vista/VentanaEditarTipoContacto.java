package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.TipoContactoDTO;
import presentacion.controlador.ControladorBotones;

public class VentanaEditarTipoContacto extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	JScrollPane spTiposContactos;
	private JTable tablaTiposDeContactos;
	private DefaultTableModel modelTiposContactos;
	private ControladorBotones controlador;
	public Object getTxtId;
	private JButton boton;
	private  String[] nombreColumnasTC = {"Nombre"};


	public VentanaEditarTipoContacto(VentanaTipoContacto v,ControladorBotones controlador) 
	{
		super(v);
		this.controlador = controlador;
		this.setTitle("Tipos de Contactos");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 300);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 500, 300);
		contentPane.add(panel);
		panel.setLayout(null);
		
		spTiposContactos = new JScrollPane();
		panel.add(spTiposContactos);
		
		modelTiposContactos = new DefaultTableModel(null,nombreColumnasTC);
		tablaTiposDeContactos = new JTable(modelTiposContactos);
		
		tablaTiposDeContactos.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tablaTiposDeContactos.setModel(new ModeloTabla(tablaTiposDeContactos.getModel()));
		
		
		spTiposContactos.setViewportView(tablaTiposDeContactos);
		
		JLabel lblCalle = new JLabel("Nombre");
		lblCalle.setBounds(10, 11, 113, 14);
		panel.add(lblCalle);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(150, 160, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		
		boton = new JButton("Editar");
		boton.addActionListener(this.controlador);
		boton.setBounds(200, 200, 89, 23);
		panel.add(boton);
	
		
//		panel.setBounds(0, 0, frame.getWidth()-16, frame.getHeight()-38);
		spTiposContactos.setBounds(10, 11, panel.getWidth()-50, panel.getHeight()-160);
		tablaTiposDeContactos.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		this.setVisible(true);
	}
	
	public void CargarTipoContacto(TipoContactoDTO tc){
				
		this.setTitle("Editar Tipo De Contacto");
		boton.setText("Editar");
		
		setTxtNombre(tc.getNombre());
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public void setTxtNombre(String nombre){
		
		txtNombre.setText(nombre);
	}
	
	
	public JButton getBoton() 
	{
		return boton;
	}
	
	public DefaultTableModel getModelTiposDeContactos() 
	{
		return modelTiposContactos;
	}
	public String[] getNombreColumnasTC() 
	{
		return nombreColumnasTC;
	}
	
	public JTable getTablaTiposDeContactos()
	{
		return tablaTiposDeContactos;
	}

}