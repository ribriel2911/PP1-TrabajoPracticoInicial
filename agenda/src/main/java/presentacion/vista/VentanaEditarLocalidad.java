package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.LocalidadDTO;
import presentacion.controlador.ControladorBotones;

public class VentanaEditarLocalidad extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	JScrollPane spLocalidades;
	private JTable tablaLocalidades;
	private DefaultTableModel modelLocalidades;
	private ControladorBotones controlador;
	public Object getTxtId;
	private JButton boton;
	private  String[] nombreColumnasL = {"Nombre"};


	public VentanaEditarLocalidad(VentanaLocalidad v,ControladorBotones controlador) 
	{
		super(v);
		this.controlador = controlador;
		this.setTitle("Localidades");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 300);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 500, 300);
		contentPane.add(panel);
		panel.setLayout(null);
		
		spLocalidades = new JScrollPane();
		panel.add(spLocalidades);
		
		modelLocalidades = new DefaultTableModel(null,nombreColumnasL);
		tablaLocalidades = new JTable(modelLocalidades);
		
		tablaLocalidades.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tablaLocalidades.setModel(new ModeloTabla(tablaLocalidades.getModel()));
		
		
		spLocalidades.setViewportView(tablaLocalidades);
		
		JLabel lblCalle = new JLabel("Nombre");
		lblCalle.setBounds(10, 11, 113, 14);
		panel.add(lblCalle);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(150, 160, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		
		boton = new JButton("Editar");
		boton.addActionListener(this.controlador);
		boton.setBounds(200, 200, 89, 23);
		panel.add(boton);
	
		
//		panel.setBounds(0, 0, frame.getWidth()-16, frame.getHeight()-38);
		spLocalidades.setBounds(10, 11, panel.getWidth()-50, panel.getHeight()-160);
		tablaLocalidades.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		this.setVisible(true);
	}
	
	public void CargarLocalidad(LocalidadDTO l){
				
		this.setTitle("Editar Localidad");
		boton.setText("Editar");
		
		setTxtNombre(l.getNombre());
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public void setTxtNombre(String nombre){
		
		txtNombre.setText(nombre);
	}
	
	
	public JButton getBoton() 
	{
		return boton;
	}
	
	public DefaultTableModel getModelLocalidades() 
	{
		return modelLocalidades;
	}
	public String[] getNombreColumnasL() 
	{
		return nombreColumnasL;
	}
	
	public JTable getTablaLocalidades()
	{
		return tablaLocalidades;
	}
}