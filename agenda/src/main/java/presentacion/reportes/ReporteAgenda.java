package presentacion.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import dto.PersonaDTO;

public class ReporteAgenda
{
	private JasperReport reporteJasper;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(ReporteAgenda.class);
	private List<PersonaDTO> personas;
	private JRDataSource reportes;
	private Map<String, Object> parametersMap;
	
	//Recibe la lista de personas para armar el reporte
    public ReporteAgenda(List<PersonaDTO> list)
    {
    	this.personas = list;
    	this.parametersMap = new HashMap<String, Object>();
    	
    	OrdenarPersonas();
    	
    	GenerarReportes();
    	
    	CargarJasperReport();
    }       
    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
    private void CargarJasperReport(){
    	
    	try	{
			this.reporteJasper = (JasperReport) JRLoader.loadObjectFromFile( "reportes" + File.separator + "ReporteAgenda.jasper" );
			this.reporteLleno = JasperFillManager.fillReport(this.reporteJasper, parametersMap,this.reportes);
    		log.info("Se cargó correctamente el reporte");
		}
		catch( JRException ex ) 
		{
			log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgenda.Jasper", ex);
		}
    }
    
    private void GenerarReportes(){
    	
    	ArrayList<ReporteMensual> ret = new ArrayList<ReporteMensual>();
    	String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre","sin cumpleaños"};
    	this.parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
    	
    	for(String s : meses){
    		ReporteMensual rep = new ReporteMensual(s);
    		ret.add(rep);
    		this.parametersMap.put(s, 0);
    	}
    	
    	this.parametersMap.put("Total",personas.size());
    	
    	for(PersonaDTO p : this.personas){
    		
    		int mes = 12;
    		
    		if(!p.getFechaCumpleaños().replace(" ", "").equals("")){
	    		if(p.getFechaCumpleaños().charAt(2) == ' ')
	        		for(int i=0;i<12;i++){
	        			if(p.getFechaCumpleaños().contains(meses[i]))
	        				mes = i;
	        		}
	    		else
	    			mes = Integer.parseInt(p.getFechaCumpleaños().substring(5, 7)) - 1;
    		}
    		ret.get(mes).addPersona(p);
    	}
    	
    	ArrayList<ReporteMensual> vacios = new ArrayList<ReporteMensual>();
    	
    	for(ReporteMensual r : ret)
    		if(r.getCantidadPersonas()==0) vacios.add(r);
    	
    	ret.removeAll(vacios);
    	this.reportes =  new JRBeanCollectionDataSource(ret);
    	
    	for(ReporteMensual r : ret){
    		
        	this.parametersMap.replace(r.getMes(), r.getCantidadPersonas());
    	}
    }
   
    private void OrdenarPersonas(){
    	
    	Collections.sort(this.personas, (p,q) -> {
    		
    		String LocalidadP = p.getTipoContacto().getNombre();
    		String LocalidadQ = q.getTipoContacto().getNombre();
    		
    		return LocalidadP.compareTo(LocalidadQ);
    	});
    }
}	