package presentacion.reportes;

import java.util.ArrayList;

import dto.PersonaDTO;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ReporteMensual {
	
	String mes;
	ArrayList<PersonaDTO> personas;

	ReporteMensual(String mes){
		
		this.mes = mes;
		personas = new ArrayList<PersonaDTO>();
	}
	
	void addPersona(PersonaDTO p){
		
		personas.add(p);
	}
	
	public String getMes(){
		
		return mes;
	}
	
	public int getCantidadPersonas(){
		return personas.size();
	}
	
	public JRDataSource getPersonasDS(){
		
		return new JRBeanCollectionDataSource(personas);
	}
}
