package presentacion.controlador;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class ControladorResize implements ComponentListener{
	
	Controlador controller;
	
	public ControladorResize(Controlador controller){
		
		this.controller = controller;
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		
		this.controller.vista.resize();
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

}
