package presentacion.controlador;

import java.awt.TrayIcon.MessageType;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.swing.JOptionPane;

public class Validador {
	
	public boolean validarCamposPersona(String nombre, String telefono, String calle, String altura, String piso, String dpto, String mail, String cumpleaños){
		if(validarNombre(nombre)==true &&
		   validarTelefono(telefono)==true && validarCalle(calle)==true &&
		   validarAltura(altura)==true && validarPiso(piso)==true && 
		   validarDpto(dpto)==true && validarMail(mail)==true &&
		   vacio(cumpleaños)==false){
			return true;
		}
		return false;
	}
	
	public boolean vacio(String string){
		if(string.isEmpty()){
			JOptionPane.showMessageDialog(null, "No se admiten campos vacios");

			return true;
		}
		return false;
	}
	
	public boolean validarNombre(String nombre){
		if(vacio(nombre)==false && esNumerico(nombre)==false && maxChars(nombre)){
			return true;
		}
		JOptionPane.showMessageDialog(null, "El campo nombre es obligatorio y no debe ser numerico");
		return false;
	}
	
	public boolean validarTelefono(String telefono){
		if(vacio(telefono)==false && esNumerico(telefono)==true && maxChars(telefono)){
			return true;
		}
		JOptionPane.showMessageDialog(null, "El campo telefono es obligatorio y debe ser numerico");
		return false;
	}
	
	public boolean validarCalle(String calle){
		if(vacio(calle)==false && maxChars(calle)){
			return true;
		}
		JOptionPane.showMessageDialog(null, "El campo calle es obligatorio");
		return false;
	}
	
	public boolean validarAltura(String altura){
		if(vacio(altura)==false&& esNumerico(altura)==true && maxChars(altura)){
			return true;
		}
		JOptionPane.showMessageDialog(null, "El campo altura es obligatorio y debe ser numerico");
		return false;
	}
	
	public boolean validarPiso(String piso){
		if(vacio(piso)==false&& esNumerico(piso)==true && maxChars(piso)){
			return true;
		}
		JOptionPane.showMessageDialog(null, "El campo piso es obligatorio y debe ser numerico");
		return false;
	}
	
	public boolean validarDpto(String dpto){
		if(vacio(dpto)==false&& esNumerico(dpto)==true && maxChars(dpto)){
			return true;
		}
		JOptionPane.showMessageDialog(null, "El campo Dpto es obligatorio y debe ser numerico","Campo Invalido",JOptionPane.ERROR_MESSAGE);
		return false;
	}
	
	public boolean validarMail(String mail){
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");	
		Matcher mather = pattern.matcher(mail);
		if (mather.find() == true && maxChars(mail)) {
	        return true;
	    } else {
	    
	        return false;
	    }
	}
	
	public boolean maxChars(String valor){
		
		return(valor.length()<=50);
	}
	
	public boolean esNumerico(String valor){     
		
			int i=0;
			String s;
			
			while(i<valor.length()){
				
				if(i+9<valor.length())
					s = valor.substring(i, i+9);
				else
					{s = valor.substring(i, valor.length());}
				
				try {
					Integer.parseInt(s);
				}
				catch (NumberFormatException nfe){
					return false;
				}
				i+=9;
			}
			
			return true;
		
	}
   
}