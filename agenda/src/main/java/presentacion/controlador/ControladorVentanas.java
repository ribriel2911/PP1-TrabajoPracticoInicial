package presentacion.controlador;

import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ControladorVentanas extends WindowAdapter{
	
	Window padre;
	
	public ControladorVentanas(Window padre){
		
		super();
		this.padre=padre;
	}
	
	@Override
    public void windowClosed(WindowEvent e) {
        
		this.padre.setEnabled(true);
		this.padre.toFront();
    }
}
