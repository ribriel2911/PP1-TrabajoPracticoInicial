package presentacion.controlador;

import java.util.List;

import modelo.Agenda;
import presentacion.vista.VentanaEditarLocalidad;
import presentacion.vista.VentanaEditarTipoContacto;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaTipoContacto;
import presentacion.vista.Vista;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class Controlador
{
		Vista vista;
		List<PersonaDTO> personas_en_tabla;
		List<LocalidadDTO> localidades_en_tabla;
		List<TipoContactoDTO> tipos_en_tabla;
		VentanaPersona ventanaAgregar;
		VentanaPersona ventanaEditar;
		VentanaLocalidad ventanaLocalidad;
		VentanaEditarLocalidad ventanaEditarLocalidad;
		VentanaTipoContacto ventanaTipoContacto;
		VentanaEditarTipoContacto ventanaEditarTipoContacto;
		Agenda agenda;
		Validador validador;
		private ControladorTabla cnTable;
		private ControladorBotones cnBotones;
		private ControladorResize cnResize;
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			this.cnTable = new ControladorTabla(this);
			this.cnBotones = new ControladorBotones(this);
			this.cnResize = new ControladorResize(this);
			this.vista.getBtnAgregar().addActionListener(cnBotones);
			this.vista.getBtnEditar().addActionListener(cnBotones);
			this.vista.getBtnAgregarLocaliadad().addActionListener(cnBotones);
			this.vista.getBtnAgregarTipoDeContacto().addActionListener(cnBotones);
			this.vista.getBtnBorrar().addActionListener(cnBotones);
			this.vista.getBtnReporte().addActionListener(cnBotones);
			this.vista.getTablaPersonas().getSelectionModel().addListSelectionListener(cnTable);
			this.vista.getFrame().addComponentListener(cnResize);
			this.agenda = agenda;
			this.personas_en_tabla = null;
		}
		
		public void inicializar()
		{
			this.llenarTabla();
			//this.llenarTablaL();
			this.vista.show();
		}
		
		void llenarTabla()
		{
			this.vista.getModelPersonas().setRowCount(0); //Para vaciar la tabla
			this.vista.getModelPersonas().setColumnCount(0);
			this.vista.getModelPersonas().setColumnIdentifiers(this.vista.getNombreColumnas());
			this.personas_en_tabla = agenda.obtenerPersonas();
			
			for (int i = 0; i < this.personas_en_tabla.size(); i ++)
			{
				System.out.print(personas_en_tabla.get(i));
				Object[] fila = {this.personas_en_tabla.get(i).getNombre(), this.personas_en_tabla.get(i).getTipoContacto().getNombre().replace("default", ""), this.personas_en_tabla.get(i).getTelefono(), this.personas_en_tabla.get(i).getCalle()+ " " + this.personas_en_tabla.get(i).getAltura(),this.personas_en_tabla.get(i).getPiso(),this.personas_en_tabla.get(i).getDpto(), this.personas_en_tabla.get(i).getLocalidad().getNombre().replace("default", ""), this.personas_en_tabla.get(i).getMail(), this.personas_en_tabla.get(i).getFechaCumpleaños()};
				//Object[] fila = {this.personas_en_tabla.get(i).getNombre(), this.personas_en_tabla.get(i).getTipoContacto().getNombre().replace("default", ""), this.personas_en_tabla.get(i).getTelefono(), this.personas_en_tabla.get(i).getCalle()+ " " + this.personas_en_tabla.get(i).getAltura(),this.personas_en_tabla.get(i).getPiso(),this.personas_en_tabla.get(i).getDpto(), this.personas_en_tabla.get(i).getLocalidad().getNombre().replace("default", "")};
				this.vista.getModelPersonas().addRow(fila);
			}
		}
		
	    void llenarTablaLocalidades()
		{	
			this.ventanaLocalidad.getModelLocalidades().setRowCount(0); //Para vaciar la tabla
			this.ventanaLocalidad.getModelLocalidades().setColumnCount(0);
			this.ventanaLocalidad.getModelLocalidades().setColumnIdentifiers(this.ventanaLocalidad.getNombreColumnasL());
			
			this.localidades_en_tabla = agenda.obtenerLocalidades();
			
			for (int i = 1; i < this.localidades_en_tabla.size(); i ++)
			{
				Object[] fila = {this.localidades_en_tabla.get(i).getNombre()};
				this.ventanaLocalidad.getModelLocalidades().addRow(fila);
			}
		}
	    
		void llenarTablaTiposDeContactos(){
			
			this.ventanaTipoContacto.getModelTiposDeContactos().setRowCount(0); //Para vaciar la tabla
			this.ventanaTipoContacto.getModelTiposDeContactos().setColumnCount(0);
			this.ventanaTipoContacto.getModelTiposDeContactos().setColumnIdentifiers(this.ventanaTipoContacto.getNombreColumnasTC());
			
			this.tipos_en_tabla = agenda.obtenerTiposContactos();
			
			for (int i = 1; i < this.tipos_en_tabla.size(); i ++)
			{
				Object[] fila = { this.tipos_en_tabla.get(i).getNombre()};
				this.ventanaTipoContacto.getModelTiposDeContactos().addRow(fila);
			}
		}
}
