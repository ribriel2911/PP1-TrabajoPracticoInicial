package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaEditarLocalidad;
import presentacion.vista.VentanaEditarTipoContacto;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaTipoContacto;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class ControladorBotones implements ActionListener{
	
	Controlador controller;
	Validador validador;
	
	public ControladorBotones(Controlador controller){
		
		this.controller = controller;
		this.validador = new Validador();
	}
	
	
	private void llenarTablaL(VentanaPersona v)
	{	
		this.controller.vista.getModelLocalidades().setRowCount(0); //Para vaciar la tabla
		this.controller.vista.getModelLocalidades().setColumnCount(0);
		this.controller.vista.getModelLocalidades().setColumnIdentifiers(this.controller.vista.getNombreColumnasL());
		
		this.controller.localidades_en_tabla = controller.agenda.obtenerLocalidades();
		
		for (int i = 0; i < this.controller.localidades_en_tabla.size(); i ++)
		{
			Object[] fila = {this.controller.localidades_en_tabla.get(i).getNombre()};
			this.controller.vista.getModelLocalidades().addRow(fila);
			v.addToCombo(this.controller.localidades_en_tabla.get(i).getNombre().replace("default", ""));
		}
	}
	
	private void LlenarTablaT(VentanaPersona v){
		
		this.controller.vista.getModelTiposDeContactos().setRowCount(0); //Para vaciar la tabla
		this.controller.vista.getModelTiposDeContactos().setColumnCount(0);
		this.controller.vista.getModelTiposDeContactos().setColumnIdentifiers(this.controller.vista.getNombreColumnasTC());
		
		this.controller.tipos_en_tabla = controller.agenda.obtenerTiposContactos();
		
		for (TipoContactoDTO t : this.controller.tipos_en_tabla)
		{
			Object[] fila = { t.getNombre()};
			this.controller.vista.getModelTiposDeContactos().addRow(fila);
			v.addToCombo2(t.getNombre().replace("default", ""));
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		//VISTA

		if(e.getSource() == this.controller.vista.getBtnAgregar())
		{
			this.controller.ventanaAgregar = new VentanaPersona(this.controller.vista,this);
			this.llenarTablaL(this.controller.ventanaAgregar);
			this.LlenarTablaT(this.controller.ventanaAgregar);
			
			this.controller.ventanaAgregar.addWindowListener(new ControladorVentanas(this.controller.vista.getFrame()));
			this.controller.vista.getFrame().setEnabled(false);
		}
		
		else if(e.getSource() == this.controller.vista.getBtnEditar()){
			
			int fila_seleccionada = this.controller.vista.getTablaPersonas().getSelectedRow();
			if(fila_seleccionada >= 0){
				
				PersonaDTO persona_seleccionada = this.controller.personas_en_tabla.get(fila_seleccionada);
				
				this.controller.ventanaEditar = new VentanaPersona(this.controller.vista,this);
				this.llenarTablaL(this.controller.ventanaEditar);
				this.LlenarTablaT(this.controller.ventanaEditar);
				this.controller.ventanaEditar.CargarPersona(persona_seleccionada);
				
				this.controller.ventanaEditar.addWindowListener(new ControladorVentanas(this.controller.vista.getFrame()));
				this.controller.vista.getFrame().setEnabled(false);
			}
		}
		
		else if(e.getSource() == this.controller.vista.getBtnAgregarLocaliadad())
		{
			this.controller.ventanaLocalidad = new VentanaLocalidad(this.controller.vista,this);
			this.controller.llenarTablaLocalidades();
			
			this.controller.ventanaLocalidad.addWindowListener(new ControladorVentanas(this.controller.vista.getFrame()));
			this.controller.vista.getFrame().setEnabled(false);
		}
		
		else if(e.getSource() == this.controller.vista.getBtnAgregarTipoDeContacto())
		{
			this.controller.ventanaTipoContacto = new VentanaTipoContacto(this.controller.vista,this);
			this.controller.llenarTablaTiposDeContactos();
			
			this.controller.ventanaTipoContacto.addWindowListener(new ControladorVentanas(this.controller.vista.getFrame()));
			this.controller.vista.getFrame().setEnabled(false);
		}
		
		else if(e.getSource() == this.controller.vista.getBtnBorrar())
		{
			int[] filas_seleccionadas = this.controller.vista.getTablaPersonas().getSelectedRows();
			for (int fila:filas_seleccionadas)
			{
				this.controller.agenda.borrarPersona(this.controller.personas_en_tabla.get(fila));
			}
			
			this.controller.llenarTabla();
		}
		else if(e.getSource() == this.controller.vista.getBtnReporte())
		{				
			ReporteAgenda reporte = new ReporteAgenda(controller.agenda.obtenerPersonas());
			reporte.mostrar();				
		}
	
		
		//VENTANA AGREGAR
		else if(this.controller.ventanaAgregar != null && e.getSource() == this.controller.ventanaAgregar.getBoton()){
			if(validador.validarCamposPersona(this.controller.ventanaAgregar.getTxtNombre().getText(), this.controller.ventanaAgregar.getTxtTelefono().getText(), this.controller.ventanaAgregar.getTxtCalle().getText(), this.controller.ventanaAgregar.getTxtAltura().getText(), this.controller.ventanaAgregar.getTxtPiso().getText(), this.controller.ventanaAgregar.getTxtDpto().getText(),this.controller.ventanaAgregar.getTxtEmail().getText(),this.controller.ventanaAgregar.getDateCumpleaños().getText().toString())==true){
				PersonaDTO nuevaPersona = new PersonaDTO(0,Localidad(controller.ventanaAgregar.getCombo().getSelectedItem().toString()),TipoContacto(this.controller.ventanaAgregar.getCombo2().getSelectedItem().toString()),this.controller.ventanaAgregar.getTxtNombre().getText(), this.controller.ventanaAgregar.getTxtTelefono().getText(), this.controller.ventanaAgregar.getTxtCalle().getText(), this.controller.ventanaAgregar.getTxtAltura().getText(), this.controller.ventanaAgregar.getTxtPiso().getText(), this.controller.ventanaAgregar.getTxtDpto().getText(),this.controller.ventanaAgregar.getTxtEmail().getText(), this.controller.ventanaAgregar.getDateCumpleaños().toString());
				
				this.controller.agenda.agregarPersona(nuevaPersona);
				this.controller.llenarTabla();
				this.controller.ventanaAgregar.dispose();	
			}
		}
		
		
		//VENTANA EDITAR
		else if(this.controller.ventanaEditar != null && e.getSource() == this.controller.ventanaEditar.getBoton()){
			if(validador.validarCamposPersona(this.controller.ventanaEditar.getTxtNombre().getText(), this.controller.ventanaEditar.getTxtTelefono().getText(), this.controller.ventanaEditar.getTxtCalle().getText(), this.controller.ventanaEditar.getTxtAltura().getText(), this.controller.ventanaEditar.getTxtPiso().getText(), this.controller.ventanaEditar.getTxtDpto().getText(),this.controller.ventanaEditar.getTxtEmail().getText(),this.controller.ventanaEditar.getDateCumpleaños().getText())==true){
				PersonaDTO nuevaPersona = new PersonaDTO(0,Localidad(this.controller.ventanaEditar.getCombo().getSelectedItem().toString()),TipoContacto(this.controller.ventanaEditar.getCombo2().getSelectedItem().toString()),this.controller.ventanaEditar.getTxtNombre().getText(), controller.ventanaEditar.getTxtTelefono().getText(), controller.ventanaEditar.getTxtCalle().getText(), controller.ventanaEditar.getTxtAltura().getText(), controller.ventanaEditar.getTxtPiso().getText(), controller.ventanaEditar.getTxtDpto().getText(), controller.ventanaEditar.getTxtEmail().getText(), controller.ventanaEditar.getDateCumpleaños().toString());
	
				int fila_seleccionada = this.controller.vista.getTablaPersonas().getSelectedRow();
				if (fila_seleccionada >= 0){
					
					PersonaDTO persona_seleccionada = this.controller.personas_en_tabla.get(fila_seleccionada);
					this.controller.agenda.editarPersona(persona_seleccionada, nuevaPersona);
					this.controller.llenarTabla();
					this.controller.ventanaEditar.dispose();
				}
			}
	
		}
		
		
		
		//VENTANA LOCALIDAD
		else if(this.controller.ventanaLocalidad != null && e.getSource() == this.controller.ventanaLocalidad.getBtnAgregar())
		{
			if(validador.vacio(this.controller.ventanaLocalidad.getTxtNombre().getText())==false){
				LocalidadDTO nuevaLocalidad = new LocalidadDTO(0,this.controller.ventanaLocalidad.getTxtNombre().getText());
				this.controller.agenda.agregarLocalidad(nuevaLocalidad);
				this.controller.ventanaLocalidad.dispose();
			}
		}
		
		else if(this.controller.ventanaLocalidad != null && e.getSource() == this.controller.ventanaLocalidad.getBtnBorrar())
		{
			int[] filas_seleccionadas = this.controller.ventanaLocalidad.getTablaLocalidades().getSelectedRows();

			for (int fila:filas_seleccionadas)
			{
				this.controller.agenda.borrarLocalidad(this.controller.localidades_en_tabla.get(fila+1));
			}
			this.controller.llenarTabla();
			this.controller.llenarTablaLocalidades();
		}
		
		else if(this.controller.ventanaLocalidad != null && e.getSource() == this.controller.ventanaLocalidad.getBtnEditar()){
			
			int fila_seleccionada = this.controller.ventanaLocalidad.getTablaLocalidades().getSelectedRow();
			if(fila_seleccionada >= 0){
				
				LocalidadDTO localidad_seleccionada = this.controller.localidades_en_tabla.get(fila_seleccionada+1);
				
				this.controller.ventanaEditarLocalidad = new VentanaEditarLocalidad(this.controller.ventanaLocalidad,this);
				this.controller.ventanaEditarLocalidad.CargarLocalidad(localidad_seleccionada);
				
				this.controller.ventanaEditarLocalidad.addWindowListener(new ControladorVentanas(this.controller.ventanaLocalidad));
				this.controller.ventanaLocalidad.setEnabled(false);
			}
		}
		
		else if(this.controller.ventanaEditarLocalidad != null && e.getSource() == this.controller.ventanaEditarLocalidad.getBoton())
		{
			if(validador.vacio(this.controller.ventanaEditarLocalidad.getTxtNombre().getText())==false){

			LocalidadDTO nuevaLocalidad = new LocalidadDTO(0,this.controller.ventanaEditarLocalidad.getTxtNombre().getText());
			int fila_seleccionada = this.controller.ventanaLocalidad.getTablaLocalidades().getSelectedRow();
			if (fila_seleccionada >= 0){
				
				LocalidadDTO localidad_seleccionada = this.controller.localidades_en_tabla.get(fila_seleccionada+1);
				this.controller.agenda.editarLocalidad(localidad_seleccionada, nuevaLocalidad);
				this.controller.llenarTablaLocalidades();
				this.controller.llenarTabla();
				this.controller.ventanaEditarLocalidad.dispose();
				}
			}
		}
		
		//VENTANA TIPO CONTACTO
		
		else if(this.controller.ventanaTipoContacto != null && e.getSource() == this.controller.ventanaTipoContacto.getBtnAgregar())
		{
			if(validador.vacio(this.controller.ventanaTipoContacto.getTxtTipo().getText())==false){

				TipoContactoDTO nuevoTipoContacto = new TipoContactoDTO(0,this.controller.ventanaTipoContacto.getTxtTipo().getText());
				this.controller.agenda.agregarTipoContacto(nuevoTipoContacto);;
				this.controller.ventanaTipoContacto.dispose();
			}
		}
		else if(this.controller.ventanaTipoContacto != null && e.getSource() == this.controller.ventanaTipoContacto.getBtnBorrar())
		{
			int[] filas_seleccionadas = this.controller.ventanaTipoContacto.getTablaTiposDeContactos().getSelectedRows();
			for (int fila:filas_seleccionadas)
			{
				this.controller.agenda.borrarTipoContacto(this.controller.tipos_en_tabla.get(fila+1));
			}
			this.controller.llenarTabla();
			this.controller.llenarTablaTiposDeContactos();
		}
		
		else if(this.controller.ventanaTipoContacto != null && e.getSource() == this.controller.ventanaTipoContacto.getBtnEditar()){
			
			int fila_seleccionada = this.controller.ventanaTipoContacto.getTablaTiposDeContactos().getSelectedRow();
			if(fila_seleccionada >= 0){
				
				TipoContactoDTO tipoDeContacto_seleccionado = this.controller.tipos_en_tabla.get(fila_seleccionada+1);
				
				this.controller.ventanaEditarTipoContacto = new VentanaEditarTipoContacto(this.controller.ventanaTipoContacto,this);
				this.controller.ventanaEditarTipoContacto.CargarTipoContacto(tipoDeContacto_seleccionado);
				
				this.controller.ventanaEditarTipoContacto.addWindowListener(new ControladorVentanas(this.controller.ventanaTipoContacto));
				this.controller.ventanaTipoContacto.setEnabled(false);
			}
		}
		
		else if(this.controller.ventanaEditarTipoContacto != null && e.getSource() == this.controller.ventanaEditarTipoContacto.getBoton())
		{
			if(validador.vacio(this.controller.ventanaEditarTipoContacto.getTxtNombre().getText())==false){
				TipoContactoDTO nuevoTipoContacto = new TipoContactoDTO(0,this.controller.ventanaEditarTipoContacto.getTxtNombre().getText());
				
				int fila_seleccionada = this.controller.ventanaTipoContacto.getTablaTiposDeContactos().getSelectedRow();
				if (fila_seleccionada >= 0){
					
					TipoContactoDTO tipoDeContacto_seleccionado = this.controller.tipos_en_tabla.get(fila_seleccionada+1);
					this.controller.agenda.editarTipoContacto(tipoDeContacto_seleccionado, nuevoTipoContacto);
					this.controller.llenarTablaTiposDeContactos();
					this.controller.llenarTabla();
					this.controller.ventanaEditarTipoContacto.dispose();
				}
			}
		}
	}
	
	private TipoContactoDTO TipoContacto(String name){
		for(TipoContactoDTO t : this.controller.tipos_en_tabla){
			if (t.getNombre().equals(name)) return t;
		}
		return this.controller.tipos_en_tabla.get(0);
	}

	private LocalidadDTO Localidad(String name) {
		for(LocalidadDTO l : this.controller.localidades_en_tabla){
			if (l.getNombre().equals(name)) return l;
		}
		return this.controller.localidades_en_tabla.get(0);
	}
}
