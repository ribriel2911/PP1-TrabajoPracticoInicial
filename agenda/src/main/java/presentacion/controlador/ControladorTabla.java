package presentacion.controlador;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import dto.PersonaDTO;

public class ControladorTabla implements ListSelectionListener{
	
	Controlador controler;
	
	public ControladorTabla(Controlador controler){
		
		this.controler = controler;
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		
		if(this.controler.ventanaEditar != null && e.getSource() == this.controler.vista.getTablaPersonas().getSelectionModel()){
			
			int fila_seleccionada = this.controler.vista.getTablaPersonas().getSelectedRow();
			
			if(fila_seleccionada >= 0){
				
				PersonaDTO persona_seleccionada = this.controler.personas_en_tabla.get(fila_seleccionada);
				this.controler.ventanaEditar.CargarPersona(persona_seleccionada);
			}
		}
	}

}
